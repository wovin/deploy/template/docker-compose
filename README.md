# [ucan-host-ipfs](https://gitlab.com/wovin/deploy/ipfs) template for docker-compose hosting

This repo is part of [ucan-host-ipfs](https://gitlab.com/wovin/deploy/ipfs), and contains a [copier](https://copier.readthedocs.io/) template, which you can use by running:
```console
nix run nixpkgs#copier copy gl:wovin/deploy/template/docker-compose .
```
(if you don't want to use [nix](https://zero-to-nix.com/start/install), you can [install copier](https://copier.readthedocs.io/en/stable/#installation) in a different way)

#### Or you can **check out & download [generated templates with default values](https://gitlab.com/wovin/deploy/template/docker-compose/-/jobs/artifacts/main/browse/generated?job=generate-templates)**.
